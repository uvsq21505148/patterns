import java.util.Deque;

public class CommandUndo implements Command {
	
	private Deque<Command> commandes;
	
	public CommandUndo(Deque<Command> commandes) {
		this.commandes = commandes;
	}
	
	@Override
	public void execute() {
		commandes.pop();
		//Normalement on veut stocker le resultat de pop dans une variable Command
		//Ensuite on code des CommandInverse pour apply et save et on l'exécute ici
	}
}
