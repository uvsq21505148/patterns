public class CommandQuit implements Command {
	
	public CommandQuit() {}
	
	@Override
	public void execute() {
		System.exit(0);
	}
}
