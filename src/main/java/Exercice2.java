public enum Exercice2 {
	ENVIRONNEMENT;
	
	public void run(String[] args) {
		Command cmd;
		MoteurRPN moteur = new MoteurRPN();
		SaisieRPN saisie = new SaisieRPN(moteur);
		
		while(true) {
			cmd = saisie.nextCommand();
			cmd.execute();
			System.out.println(moteur.getOperandes());
		}
	}
}
