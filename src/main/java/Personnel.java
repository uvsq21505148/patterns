import java.util.ArrayList;
import java.time.LocalDate;

public class Personnel implements InfosPersonnel{
	private final String nom;
	private final String prenom;
	private final String fonction;
	private final LocalDate dateNaissance;
	private final ArrayList<String> numerosTel;
	
	public static class PersonnelBuilder {
		//Obligatoire
		private final String nom;
		private final String prenom;
		private final String fonction;
		private final LocalDate dateNaissance;
		//Optionnel
		private ArrayList<String> numerosTel = new ArrayList<String>();
		
		public PersonnelBuilder(String nom, String prenom, String fonction, LocalDate dateNaissance) {
			this.nom = nom;
			this.prenom = prenom;
			this.fonction = fonction;
			this.dateNaissance = dateNaissance;
		}
		
		public PersonnelBuilder numeroTel(String numero) {
			numerosTel.add(numero);
			return this;
		}
		
		public Personnel build() {
			return new Personnel(this);
		}
	}
	
	private Personnel(PersonnelBuilder builder) {
		nom 			= builder.nom;
		prenom 			= builder.prenom;
		fonction 		= builder.fonction;
		dateNaissance	= builder.dateNaissance;
		numerosTel		= builder.numerosTel;
	}
	
	@Override
	public void affiche() {
		System.out.println(nom + " " + prenom + " " + dateNaissance.toString());
		System.out.println(fonction);
		for(String num : numerosTel) {
			System.out.println(num);
		} 
	}
}
