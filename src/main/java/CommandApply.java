public class CommandApply implements Command {
	
	private MoteurRPN moteur;
	private Operation operation;
	
	public CommandApply(MoteurRPN moteur, Operation operation) {
		this.moteur = moteur;
		this.operation = operation;
	}
	
	@Override
	public void execute() {
		double tmp = operation.eval(moteur.getOperandes().pop(), moteur.getOperandes().pop());
		moteur.getOperandes().push(tmp);
		moteur.getCommandes().push(this);
	}
}
