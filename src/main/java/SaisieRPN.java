import java.util.Scanner;

public class SaisieRPN {
	Scanner sc;
	MoteurRPN moteur;
	
	public SaisieRPN(MoteurRPN moteur) {
		this.moteur = moteur;
		sc = new Scanner(System.in);
	}
	
	public Command nextCommand() {
		String cmdStr = sc.next();
		switch(cmdStr) {
			case "undo" :
				return moteur.undo();
			case "exit" :
				return moteur.quit();
			case "+" :
				return moteur.apply(Operation.PLUS);
			case "-" :
				return moteur.apply(Operation.MOINS);
			case "*" :
				return moteur.apply(Operation.MULT);
			case "/" :
				return moteur.apply(Operation.DIV);
			default :
				return moteur.save(Double.parseDouble(cmdStr));		
		}
	 }
}
