import java.util.Deque;
import java.util.ArrayDeque;

public class MoteurRPN extends Interpreteur{
	
	private Deque<Double> operandes;
		
	public MoteurRPN() {
		operandes = new ArrayDeque<Double>();
		commandes = new ArrayDeque<Command>();
	}
	
	public Deque<Double> getOperandes() {
		return operandes;
	}
	
	public Command save(Double operande) {
		return new CommandSave(this, operande);
	}
	
	public Command apply(Operation operation) {
		return new CommandApply(this, operation);
	}
}
