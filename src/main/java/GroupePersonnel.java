import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

public class GroupePersonnel implements InfosPersonnel, Iterable<InfosPersonnel>{
	
	public List<InfosPersonnel> groupe;
	
	public GroupePersonnel() {
		 groupe = new ArrayList<InfosPersonnel>();
	 }
	
	public boolean add(InfosPersonnel p) {
		return groupe.add(p);
	}
	
	@Override
	public void affiche() {
		for(InfosPersonnel p : groupe) {
			p.affiche();
		}
	}
	
	//Parcours en profondeur On affiche les elements dans l'ordre d'insertion
	//~ @Override
	//~ public Iterator<InfosPersonnel> iterator() {
		//~ return groupe.iterator();
	//~ }
	
	//Parcours largeur On affiche tous les Personnel avant d'afficher les Groupe
	@Override
	public Iterator<InfosPersonnel> iterator() {
		List<InfosPersonnel> resultat = new ArrayList<InfosPersonnel>();
		List<GroupePersonnel> groupes = new ArrayList<GroupePersonnel>();
		GroupePersonnel tmp;
		InfosPersonnel aTraiter;
			
		groupes.add(this);
		while(!groupes.isEmpty()){
			tmp = groupes.remove(0);
			while(!tmp.groupe.isEmpty()){
				aTraiter = tmp.groupe.remove(0);
				if(aTraiter instanceof GroupePersonnel) {
					groupes.add((GroupePersonnel) aTraiter);
				}
				else {
					resultat.add(aTraiter);
				}
			}
		}
		return resultat.iterator();
	}
}
