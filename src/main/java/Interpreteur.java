import java.util.Deque;

public abstract class Interpreteur {
	protected Deque<Command> commandes;
	
	public Deque<Command> getCommandes() {return commandes;}
	
	public final Command quit() {
		return new CommandQuit();
	}
	
	public final Command undo() {
		return new CommandUndo(commandes);
	}
}
