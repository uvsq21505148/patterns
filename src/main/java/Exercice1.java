import java.time.LocalDate;

public enum Exercice1 {
	ENVIRONNEMENT;
	
	public void run(String[] args) {
		
		Personnel sonny = new Personnel.PersonnelBuilder(
		"Sonny", "Klotz",
		"Etudiant à l'UVSQ",
		LocalDate.of(1996, 06, 05))
		.numeroTel("06 51 21 11 15")
		.numeroTel("09 87 25 64 55")
		.build();
		
		Personnel larry = new Personnel.PersonnelBuilder(
		"Larry", "Page",
		"Etudiant à l'UVSQ",
		LocalDate.of(1996, 06, 05))
		.numeroTel("06 51 21 11 15")
		.numeroTel("09 87 25 64 55")
		.build();
		
		Personnel gauss = new Personnel.PersonnelBuilder(
		"Car-Friedrichl", "Gauss",
		"Enseignant à l'UVSQ",
		LocalDate.of(1958, 12, 12))
		.numeroTel("01 87 89 56 42")
		.build();
		
		Personnel euler = new Personnel.PersonnelBuilder(
		"Leonhard", "Euler",
		"Enseignant à l'UVSQ",
		LocalDate.of(1958, 12, 12))
		.build();
		
		GroupePersonnel elevesMaths = new GroupePersonnel();
		elevesMaths.add(sonny);
		elevesMaths.add(larry);
		
		GroupePersonnel profsMaths = new GroupePersonnel();
		profsMaths.add(gauss);
		profsMaths.add(euler);
		
		GroupePersonnel CoursMaths = new GroupePersonnel();
		CoursMaths.add(profsMaths);
		CoursMaths.add(elevesMaths);
		
		for(InfosPersonnel tmp : CoursMaths) {
			tmp.affiche();
		}
	}
}
