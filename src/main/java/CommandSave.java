public class CommandSave implements Command {
	
	private MoteurRPN moteur;
	private Double operande;
	
	public CommandSave(MoteurRPN moteur, Double operande) {
		this.moteur = moteur;
		this.operande = operande;
	}
	
	@Override
	public void execute() {
		moteur.getOperandes().push(operande);
		moteur.getCommandes().push(this);
	}
}
